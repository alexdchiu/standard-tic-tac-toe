def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over (board, num):
  print_board(board)
  print("GAME OVER", board[num], "has won")
  exit()

def is_row_winner(board, row_number):
  if board[(row_number-1)*3] == board[(row_number-1)*3+1] and board[(row_number-1)*3+1] == board[(row_number-1)*3+2]:
    return True

def is_col_winner(board, col_number):
  if board[col_number-1] == board[col_number+2] and board[col_number+2] == board[col_number+5]:
    return True

def is_diag_winner(board, diag_number):
  if board[diag_number*2] == board[4] and board[4] == board[8 - diag_number * 2]:
    return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board,1) == True:
        game_over (board, 0)
    elif is_row_winner(board,2) == True:
        game_over (board, 3)
    elif is_row_winner(board,3) == True:
        game_over (board, 6)
    elif is_col_winner(board,1) == True:
        game_over (board, 0)
    elif is_col_winner(board,2) == True:
        game_over (board, 1)
    elif is_col_winner(board,3) == True:
        game_over (board, 2)
    elif is_diag_winner(board,0) == True:
        game_over (board, 0)
    elif is_diag_winner(board,1) == True:
        game_over (board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
